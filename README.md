# FPCTabellenkalkulation

Ein Terminal Tabellenkalkulationsprogramm für den Free Pascal Compiler

Die genutzten Dokumentation / hilfestellungen sind in der Datei Quellen.md zu finden.

## Dokumentation sollte enthalten:
- detaillierten Problembeschreibung
- Struktogramm für einen wichtigen Abschnitt (ca. eine halbe A4-Seite)
- zeitliche Abfolge der Entwicklungsschritte
- aufgetretene Fehler und ihre Behebung
- Test der funktionsfähigen Anwendung mit Werten (ca. 3 dokumentierte Programmdurchläufe)