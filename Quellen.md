# Hilfestellungen und verwendete Quellen

## 26.01.2020
19:11 Lazarus Gitignore importiert damit Git nur noch wichtige Dateien commitet.
https://github.com/github/gitignore/blob/master/Global/Lazarus.gitignore -> .gitignore

21:45 Lazarus Unit verstanden und implementiert. Erst die deutsche Version konnte mir helfen, alle anderen haben noch "type" im Beispiel, welches das programm nicht bauen gelassen hat
https://wiki.freepascal.org/Unit/de

## 28.01.2020
23:24 Objekte für die einzelnen Zellen (mit Eigenschaften und Werten)
https://wiki.freepascal.org/Programming_Using_Objects -> tabellenrechner.pas

## 29.01.2021
17:37 Dynamischer Array für die Zellen
https://wiki.freepascal.org/Dynamic_array -> tabellenrechner.pas

23:07 Für eine Objektorientierung für die einzelnen Zellen (Versuch 2) (Branch TryUglyObjectPascal)
https://en.wikipedia.org/wiki/Object_Pascal

## 01.02.2021
18:57 Case für das verwenden des Zelleninhalttyps
https://wiki.freepascal.org/Case -> tabellenrechner.zelle.getText()

20:56 ReadKey für das Lesen von nur einem Buchstaben von der Tastatur
https://www.freepascal.org/docs-html/rtl/crt/readkey.html -> eingabe.inputLoop()

## 03.02.2021
23:56 Aufteilen (Splitten) von einem String in Teile. Mit Char als Trenner
https://www.freepascal.org/docs-html/rtl/sysutils/tstringhelper.split.html -> tabellenrechner.zelle.setVar(variable: string)
--> Wegen Kompatibilitätsproblemen verworfen

## 12.02.2021
12:27 Testen der Konvertierung von String to Real
https://www.freepascal.org/docs-html/rtl/sysutils/strtofloat.html -> tools.canBeReal(test: string);
