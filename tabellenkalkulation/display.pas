unit display;

// Diese Unit kümmert sich darum, dass im Konsolenfenster etwas erscheint
// Außerdem speichert die Unit alle Werte, die damit in Verbindung hängen und nicht zu den Werten in den Felder gehören

{$mode objfpc}{$H+}

interface

// Initialisierungsmethoden
procedure getTerminalResolutionFromUser();
procedure calcOptimalScreenRendering();

procedure writeToDisplay();

// Getters
function getCalculatedTRX(): integer;
function getCalculatedTRY(): integer;

function getTermResolutionX(): integer;
function getTermResolutionY(): integer;

function getCurrentX(): integer;
function getCurrentY(): integer;

// Cursorbewegung
procedure cursorLeft();
procedure cursorDown();
procedure cursorUp();
procedure cursorRight();

implementation

uses spreadsheetcalc, tools, SysUtils, reshelper, de;

const // Standartwerte für die Darstellung (in Zeichen)
  rowLength = 6; // 5 für Inhalt + 1 für die Markierung (z.B. '$'/'*')
  columnHeigth = 1; // Da keine zwischenstriche zwischen Zeilen sind, ist eine Zelle ein zeichen hoch
  inputRows = 2; // Wieviele Zeilen auf dem Bildschirm übrig bleiben sollen
                 // Für die Ein- und Ausgabe von Werten und Informationen
var
  termX: integer; // Horizontale Breite des Terminals
  termY: integer; // Vertikale Höhe des Terminals
  rows: integer; // Zeilen die auf dem Bildschirm dargestellt werden sollen
  rrows: integer; // Plätze im Terminal, die keine Zeilen darstellen.
  columns: integer; // Spalten die auf dem Bildschirm dargestellt werden sollen
  rcolumns: integer; // Plätze im Terminal, die keine Spalten darstellen

  x,y: integer; // Werte an denen geschrieben wird

procedure getTerminalResolutionFromUser(); // Frage den Nutzer nach der Terminal Höhe und Breite
begin
  // Nicht die schönste Lösung, aber warscheinlich die, die am einfachsten auf allen Plattformen funktioniert
  // und warscheinlich die beste, mit unserer Beschränkten Auswahl möglich war.
  writeln('Damit wir Ihnen das beste Tabellenkalkulationserlebnis bieten k', de.oe(),'nnen, ben', de.oe(),'tigen wir Ihre Terminal Gr', de.oe(), de.sz(),'e.');
  reshelper.show(); // Damit der Nutzer die Möglichkeit hat, sich mithilfe dem Reshelper das Abzählen zu erleichtern
  writeln('Bitte geben Sie diese in Zeichen an. Eine Ver', de.ae(),'nderung dieser ist sp', de.ae(),'ter nicht mehr m', de.oe(),'glich!');
  Write('Horizontale Terminalbreite: ');
  readln(termX); // Wichtig: Wert in Zeichen, nicht in Pixel
  Write('Vertikale Terminalh', de.oe(),'he: ');
  readln(termY); // Wichtig: Wert in Zeichen, nicht in Pixel
end;

procedure calcOptimalScreenRendering(); // Berechnet das optimale Format (Länge und Höhe) für die Tabelle
begin
  // Zeilen (Eine Zeile für Beschriftung + Zeilen für Ein- und Ausgabe)
  // /- die für z.B. die Beschriftung gebrauchten Zeilen und ein zeichen für den trennstrich werden hier abgezogen
  rows := (termY - 1 - inputRows) div (columnHeigth);  // Anzahl an Zeilen
  rrows := (termY - 1 - inputRows) mod (columnHeigth); // Restlichen Zeichen (die, die nicht benutzt werden)
  // \- Notiert die restlichen unbenutzten Zeichen
  // Auch wenn diese in dieser Version nicht verwendet werden

  // Spalten
  // drei Zeichen können nicht verwendet werden -> werden für Beschriftung z.B. "01|" verwendet
  columns := (termX - 3) div (rowLength + 1);  // Anzahl an Spalten
  rcolumns := (termX - 3) mod (rowLength + 1); // Restliche Zeichen, die nicht verwendet werden)
  // Notiert die restlichen unbenutzten Zeichen

  // Informiere Nutzer über die Ergebnisse
  writeln('Mit ihrer gew', de.ae(),'hlten Aufl', de.oe(),'sung k', de.oe(),'nnen Sie ', rows, ' Zeilen mit ', columns, ' Spalten verwenden');
  if columns > 26 then // 26 -> Z --> Das nächste Zeichen ist nicht AA sondern das Ascii Zeichen, das nach dem gro0en Z kommt
  begin
    writeln('WARNUNG: Die maximale Spaltenanzahl von 26 wurde ', de.ue(),'berschritten. Spalten hinter "Z" werden warscheinlich nicht per Formel ansprechbar sein. Wenn Sie dies st', de.oe(),'rt, starten Sie das Programm bitte in einer niedrigen Aufl', de.oe(),'sung neu.');
    readln(); // Damit der Nutzer die Warnung ließt
  end;

end;

function getCalculatedTRX(): integer; // Geben die ausgerechnete Anzahl von Spalten zurück
begin
  result := columns;
end;

function getCalculatedTRY(): integer; // Geben die ausgerechnete Anzahl von Zeilen zurück
begin
  result := rows;
end;

function getTermResolutionX(): integer; // Geben die die rohe Terminalauflösung zurück (in Zeichen)
begin
  result := termX;
end;

function getTermResolutionY(): integer; // Geben die die rohe Terminalauflösung zurück (in Zeichen)
begin
  result := termY;
end;

procedure writeToDisplay(); // Gibt das ganze auf dem Terminalfenster aus
var
  lineOutput: string;
  x, y: integer;
begin
  // Zeile damit der Anfang der Tabelle erkennbar ist, wenn diese vertikal zu klein sein sollte
  for x := 1 to termX do write('-');
  writeln();
  // Zuerst wir der Tabellenkopf "gedruckt"
  for y := 0 to rows do // Für die einzelnen Zeilen
  begin       // Drucke die erste Spalte
    lineOutput := tools.getStringToLength(
      IntToStr(spreadsheetcalc.getVerticalRowLabel(y)), 2) + '|';

    for x := 1 to columns do // Für die einzelnen Spalten
    begin
      if y = 0 then // wenn dies die 0te Zeile ist (Beschriftungszeile)
      begin // Drucke die Beschriftung mit Buchstaben, die über tools.getStringToLength() zurechtgeschnitten und aufgefüllt wird
        lineOutput := lineoutput + tools.getStringToLength(
          spreadsheetcalc.getHorizontalColumnLabel(x), rowLength);
        if not(x = columns) then lineOutput := lineoutput + '|'; // Drucke nur einen Trenner, wenn dies nicht die letze Spalte ist
      end
      else begin // Wenn es nicht die 0te Zeile ist (Datenzeile)
      // Frage den Wert für die jeweilige Zelle aus der tabelle ab und lasse ihn von tools.getstringtolength() zurechtkürzen und auffüllen
        lineOutput := lineoutput + tools.getStringToLength(spreadsheetcalc.getFieldText(x, y), rowLength -1);
        if (display.x = x) and (display.y = y) then lineOutput := lineoutput + '*' // Wenn sich dier Cursor hier befindet, dann füge '*' ein
        else if spreadsheetcalc.fieldHasFunction(x, y) then lineOutput := lineoutput + '$' // wenn sich hier eine Foormel und kein Cursor befindet, füge '$' ein
        else lineOutput := lineoutput + ' '; // wenn sich hier keins von den beiden oben genannten befindet, füge ' ' ein
        if not(x = columns) then lineOutput := lineoutput + '|'; // Drucke nur einen Trenner, wenn dies nicht die letze Spalte ist
      end;
    end;
    writeln(lineOutput); // Schreibe die oben zusammengesetzte Zeile ins Terminal
  end;
end;

// Bewege den Cursor

procedure cursorLeft();
begin
  if x > 1 then x -= 1;
end;

procedure cursorDown();
begin
  if y < rows then y += 1;
end;

procedure cursorUp();
begin
  if y > 1 then y -= 1;
end;

procedure cursorRight();
begin
  if x < columns then x += 1;
end;

// Anfragen nach der Cursorposition

function getCurrentX(): integer;
begin
  result := x;
end;

function getCurrentY(): integer;
begin
  result := y;
end;

initialization; // initialisiere die Klasse mit der Cursor Position (1,1)
begin
  x := 1;
  y := 1;
end;

end.

