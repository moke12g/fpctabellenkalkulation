unit spreadsheetcalc;
// Hier wird die Tabelle gespeichert, zugegriffen, abgefragt und verwaltet

// Diese Unit ist sehr, sehr lang, jedoch ist sie einfach aufgebaut
// 1. Interface
// 2. Konstantendeklaration
// 3. sFunction
// 4. sFunction Methoden
// 5. field
// 6. Field Methoden
// 7. Generelle Getter und Setter für Werte (und anderes Zeug)
// 8. Initieren der Tabelle


{$mode objfpc}{$H+}

interface
function getHorizontalColumnLabel(entry: integer): char;
function getVerticalRowLabel(entry: integer): integer;
function getFieldText(x,y: integer): string;
procedure setFieldVar(x,y: integer; variable: string);
procedure setFieldVar(x,y: integer; variable: real);
function fieldHasFunction(x,y: integer): boolean;
function isFieldReturningAnString(x,y: integer): boolean;
procedure initSpreadsheet();

implementation

uses display, sysutils, stringsplitter, tools;

const // Konstanten zum Speichern des Datentyps für field und sFunction
//fieldIsNumber = 0; // legacy
  fieldIsString = 1;
  fieldIsSpecial = 2;

  sFunctionIsArithmeticSymbol = 1;
  sFunctionIsValue = 0;
  sFunctionIsMirror = 2;

type
  sFunction = Class
    // Wertspeicher
    fieldType: shortint; // 0 -> Werte (real); 1 -> Rechenzeichen; 2 -> Verweis
    varString: String;                           //  \\\\\\\\\\\\
    varNumber: real;                             //   Nutzt den String (varString)

    // Verweis
    mirroredFieldX: integer; // Wohin verwiesen wird
    mirroredFieldY: integer;

    // Methoden
    procedure initAsArithmeticSymbol(content: String);
    procedure initAsValue(content: real);              // Zum initieren als bestimmter Datentyp
    procedure initAsMirroredField(x,y: integer);

    // Getter
    function getFieldType(): shortint;
    function getValueAsNumber(): real;
    function getValueAsString(): string;
  end;

procedure sFunction.initAsArithmeticSymbol(content: String);
begin
  fieldType := sFunctionIsArithmeticSymbol;
  varString := content;
end;

procedure sFunction.initAsValue(content: real);
begin
  fieldType := sFunctionIsValue;
  varNumber := content;
end;

procedure sFunction.initAsMirroredField(x,y: integer);
begin
  fieldType := sFunctionIsMirror;
  mirroredFieldX := x;
  mirroredFieldY := y;
end;

function sFunction.getFieldType(): shortint;
begin
  result := fieldType;
end;

function sFunction.getValueAsNumber(): real;
begin
  case fieldtype of
  sFunctionIsValue:
    result := varNumber;
  sFunctionIsMirror:
    begin
    if not(isFieldReturningAnString(mirroredFieldX, mirroredFieldY)) then result := StrToFloat(getFieldText(mirroredFieldX, mirroredFieldY))
    else result := 0; // Keine Konvertierung in eine Zahl möglich
    end;
  end; // End of Case
end;

function sFunction.getValueAsString(): String;
begin
  case fieldtype of
  sFunctionIsArithmeticSymbol:
    result := varString;
  sFunctionIsValue:
    result := floattostr(varNumber);
  sFunctionIsMirror:
    result := getFieldText(mirroredFieldX, mirroredFieldY);
  end;
end;

{ Field
  Wenn zugewiesener Wert String ist, geht er in Field.
  Wenn zugewiesener Wert Number ist, geht er in sFunction.
  Wenn mehrere Werte zugewiesen werden, geht das ganze auch in sFunction(s).
}

type
  Field = class
    // Datenspeicher
    varType: shortint; // genutzter Variablentyp
    varString: string; // type 0
  //varNumber: real;   // type 1 // Legacy
    // type 2 special
    sFunctions: Array of sFunction; // Arrayy für die sFunctions des Feldes
    // methods
    procedure init(); // zum generellen initieren

    // Getter
    function getText(): string;
    function getVar(): real;
    function getVarType(): shortint;

    // Datenspeicherkontrollvariable
    function isReturningString(): boolean;

    // Setter
    procedure setVar(variable: string);
  end;

procedure Field.init();
begin
  // Init Sachen für die Zelle
  varType := fieldIsString;
  varString := '';

end;

function Field.getText(): string;
begin
  case varType of
    fieldIsSpecial:
      result := floattostr(getVar());
    fieldIsString:
      result := varString;
  end; // End of Case
end;

function Field.isReturningString(): boolean;
begin
  result := (varType = fieldIsString);
end;

function Field.getVarType(): shortint;
begin
  result := varType;
end;

function Field.getVar(): real;
var
  i: integer;
  lastArithmeticSymbol: char; // Muss, wenn längere Rechenzeichen implementiert werden, geändert werden
begin // Ich hoffe, was hier gemacht wird ist nicht illegal
  lastArithmeticSymbol := '*';
  result := 0;
  for i := 0 to length(sFunctions) -1 do
  begin
    case sFunctions[i].getFieldType() of
      sFunctionIsArithmeticSymbol: lastArithmeticSymbol := sFunctions[i].getValueAsString()[1]; // Ursache des Problems
      sFunctionIsValue, sFunctionIsMirror: begin
      // Rechne
      if i = 0 then result := sFunctions[i].getValueAsNumber() // Setze den Wert von Int, wenn dies die erste Zahl ist
      else case lastArithmeticSymbol of
        '+': result := result + sFunctions[i].getValueAsNumber();
        '-': result := result - sFunctions[i].getValueAsNumber();
        '*': result := result * sFunctions[i].getValueAsNumber();
        '/': result := result / sFunctions[i].getValueAsNumber();
        'd': result := round(result) div round(sFunctions[i].getValueAsNumber());
        'm': result := round(result) mod round(sFunctions[i].getValueAsNumber());
        end;
      end;
    end;
  end;
end;

procedure Field.setVar(variable: string);
var
  i: integer;
begin

  if tools.containsChar(variable, ';') then begin // Wenn String ein Befehl ist
  // Splitte String nach ";"

  stringSplitter.setString(variable);
  stringSplitter.setSeparator(';');
  stringSplitter.process();

  setlength(sFunctions,stringsplitter.getLength());
  for i := 0 to (stringsplitter.getLength()-1) do
  begin
    sFunctions[i] := sFunction.create();
    if Tools.canBeReal(stringsplitter.getSplitAt(i)) then begin // Wenn Split Number ist
      sFunctions[i].initAsValue(StrToFloat(stringsplitter.getSplitAt(i)));
    end else begin // Wenn Split keine Zahl ist
      if (stringsplitter.getSplitAt(i)[1] = '$') then sFunctions[i].initAsMirroredField(Tools.getDestinationXFromText(stringsplitter.getSplitAt(i)), Tools.getDestinationYFromText(stringsplitter.getSplitAt(i)))
    else sFunctions[i].initAsArithmeticSymbol(getSplitAt(i)); // webb Split keine Zahl und kein Verweis ist
    end;
  end;
  varType := fieldIsSpecial; // sonst ist die zelle verwirrt, ob sie etwas zusammenrechnen soll

  end else begin // Wenn String keine Aufzählung ist -> Soll nur ein Wert gespeichert werden
    if Tools.canBeReal(variable) then begin // Wenn die Eingabe eine Zahl ist
      setlength(sFunctions,1);                          // TODO: Könnte man als Funktion ausklammern
      sFunctions[0] := sFunction.create();
      sFunctions[0].initAsValue(StrToFloat(variable)); // Speichere einzelne Zahl, als wenn es mehrere wären
      varType := fieldIsSpecial;
    end
    else if (variable[1] = '$') then begin // Wenn einzelner Wert ein Verweis ist
      setlength(sFunctions,1);
      sFunctions[0] := sFunction.create();
      sFunctions[0].initAsMirroredField(Tools.getDestinationXFromText(variable), Tools.getDestinationYFromText(variable));
      varType := fieldIsSpecial;
    end
        else begin // Wenn einzelner Wert kein Verweis ist, ist er ein String
     varString := variable;
     varType := fieldIsString;
        end;
    end;
end;

var
  spreadsheet: array of array of field;
  maxX, maxY: integer; // TODO: FIx benennung -> Verwirrend
  // \-----\-> Spalten- und Zeilenanzahl

function getHorizontalColumnLabel(entry: integer): char;
begin                 // -1 weil die Spalten sonnst mit B anfangen würden
  result := char(entry -1 + 65); // Addiere Argument zu Ascii Wert von A -> 25 Spalten maximal
end;

function getVerticalRowLabel(entry: integer): integer;
begin
  result := entry; // Sieht zwar nach Blödsinn aus, erleichtert aber das Fixen von Bugs,
  // außerdem muss die Display Methode so weniger machen
end;

function getFieldText(x,y: integer): string;
begin // sollte funktionieren
  if spreadsheet[x, y].isReturningString() then result := spreadsheet[x, y].getText()
  else result := floattostr(spreadsheet[x, y].getVar());
end;

function isFieldReturningAnString(x,y: integer): boolean;
begin
  result := spreadsheet[x, y].isReturningString();
end;

procedure setFieldVar(x,y: integer; variable: real);
begin
  spreadsheet[x, y].setVar(string(variable));
end;

procedure setFieldVar(x,y: integer; variable: string);
begin
  spreadsheet[x, y].setVar(variable);
end;

function fieldHasFunction(x,y: integer): boolean;
begin
  result := (spreadsheet[x, y].getVarType() = fieldIsSpecial);
end;

procedure initSpreadsheet();
var
  x,y: integer; // setze spreadsheet auf die richtigen Arraylängen und initiere die Felder
begin
  display.getTerminalResolutionFromUser();
  display.calcOptimalScreenRendering();
  maxX := display.getCalculatedTRX();
  maxY := display.getCalculatedTRY();

  //tabellenrechner.maxX := maxX;   // Wodurch die erste Spalte und Zeile leer sind -> dafür ist es Fehlerunanfälliger
  //tabellenrechner.maxY := maxY;     /////////                                        + habe nichts gefunden um dne Anfang von setLength() zu setzen
  // Auf welche Länge wird gesetzt? (0 .. maxX) oder (1 .. maxX) Danke Arthur
  setLength(spreadsheet, maxX +1, maxY +1); // Problem -> wusste nicht, wie man Arrays neu Zuweisen kann (Dynamic Arrays)
  for y := 0 to maxY do                // \-> gefragt in Konferenz 29.01.21 -> danach gelöst
  begin
    for x := 0 to maxX do
    begin
      spreadsheet[x, y] := field.create(); // Fehler
      spreadsheet[x, y].init(); // Setze alle Felder zurück
    end;
  end;
end;

end.
