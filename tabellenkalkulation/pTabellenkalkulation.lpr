program pTabellenkalkulation;

uses spreadsheetcalc, input;

begin
  writeln('FPCTabellenkalkulation');
  writeln('Moritz Kempe 2021');
  writeln('https://gitlab.com/moke12g/fpctabellenkalkulation');

  spreadsheetcalc.initSpreadSheet(); // initiere die Tabelle
  input.inputloop();                 // Gehe in Input Loop

  writeln('Created by Moritz Kempe');
  writeln('https://gitlab.com/moke12g/fpctabellenkalkulation');
  end.
