unit de;

// Klasse gibt die richtige Umlaute für die jeweilige Plattform aus
// Garantiert die richtige Darstellung auf Windows und Linux (Betriebssystemen)
// geschrieben mithilfe der Ascii Tabelle

// Die Methoden sollten selbsterklärend sein

{$mode objfpc}{$H+}

interface

function ae(): string;
function oe(): string;
function ue(): string;
function sz(): string;

implementation

uses
  Classes, SysUtils;

function ae(): string;
begin
  result :='ae';
  {$IFDEF WIN64, WIN32}
    result := char(132)
  {$ENDIF}
  {$IFDEF UNIX}
    result := 'ä';
  {$ENDIF}
end;

function oe(): string;
begin
  result :='oe';
  {$IFDEF WIN64, WIN32}
    result := char(148)
  {$ENDIF}
  {$IFDEF UNIX, LINUX}
    result := 'ö';
  {$ENDIF}
end;

function ue(): string;
begin
  result :='ue';
  {$IFDEF WIN64, WIN32}
    result := char(129)
  {$ENDIF}
  {$IFDEF UNIX, LINUX}
    result := 'ü';
  {$ENDIF}
end;

function sz(): string;
begin
  result :='ss';
  {$IFDEF WIN64, WIN32}
    result := char(225)
  {$ENDIF}
  {$IFDEF UNIX, LINUX}
    result := 'ß';
  {$ENDIF}
end;

end.

