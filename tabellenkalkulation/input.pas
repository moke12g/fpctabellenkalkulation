unit input;

{$mode objfpc}{$H+}

interface

procedure inputloop(); // generelle Programmschleife
procedure enterValueInputMode(); // Starte den Modus zum Zuweisen von Zellinhalten
procedure enterManipulation(); // Starte den Modus zum Manipulieren des Programmes (ähnlich wie Kontextmenü)

implementation

uses
  Classes, SysUtils, Display, crt, spreadsheetcalc, debugmenu, documentation;

var
  anotherTime: boolean; // Damit das Programm auch beendet werden kann

procedure inputLoop(); // der normale Loop, der auf die Nutzereingabe wartet
var
  c: char;
begin
  anotherTime := True; // Ob der Nutzer das Programm witer ausführen möchte

  while anotherTime do // Programmschleife
  begin
    display.writeToDisplay(); // Zeige Nutzer erstmal die Tabelle
    writeln('Navigieren mit WASD oder hjkl, =, !'); // Gebe Eingabe Tipp, der auf jedes Terminalfenster passen sollte
    c := readkey(); // Lese Eingabe
    case lowercase(c) of // \-> und wähle die passende Reaktion
      'h', 'a': display.cursorLeft(); // \
      'j', 's': display.cursorDown(); //  \- Verändere die Position der Auswahl
      'k', 'w': display.cursorUp();   //  /
      'l', 'd': display.cursorRight();// /
      '=', 'i': enterValueInputMode(); // Gehe in den Feld-Wert-Eingabe-Modus
      '!': enterManipulation(); // Gehe in den Manipulationsmodus (ähnlich Kontextmenü)
    end;
  end;
    // Beendet das Programm, wenn anotherTime false ist
    writeln();
end;

procedure enterValueInputMode(); // Feld-Wert-Eingabe-Modus
var
  value: string; // Eingabevariable
begin
  display.writeToDisplay(); // Zeichnet Tabelle neu, nur um sicher zu gehen
  writeln('; (Trenner), $Feld, +,-,*,/,d,m'); // Gebe Eingabe Tipp, der auf jedes Terminalfenster passen sollte
  write('='); // Weil die Tabelle dazwischen neu gezeichnet wird, wird hier nochmal ein '=' Zeichen angegeben
  readln(value); // Lese eingabe, das hinter dem '=' aber nicht das '=' Zeichen
  spreadsheetcalc.setFieldVar(display.getCurrentX,display.getCurrentY, value); // Gebe Eingabe an den Verwalter der Tabelle weiter
end;

procedure enterManipulation(); // Änlich wie Kontextmenü
var
  c: char;
begin
  display.writeToDisplay(); // Tabelle wird gezeichnet, damit hier bei der Implementation von z.B. Paste kein Fehler auftritt
  writeln('Bitte Manipulationscode eingeben (ESC / q = beende Programm)');
  write('!'); // Wie bei enterValueInputMode()
  c := readkey();

  case c of
    'q': anotherTime := false; // Beendet das Programm, weil Schleifenbedingung nicht mehr erfüllt ist
    'd': debugmenu.show(); // Zeige Debugmenu
    'h': documentation.showDocumentation(); // Zeige die Hilfe
  end;

end;

end.
