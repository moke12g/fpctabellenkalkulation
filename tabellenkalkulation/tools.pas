unit tools;
// Unit für Methoden die man halt mal braucht...

{$mode objfpc}{$H+}

interface

// Zwei String-aufpump-Methoden (einmal wählbarem Schnitt und einmal mit Schnitt)
function getStringToLength(something: string; newLength: integer; cutString: boolean): string;
function getStringToLength(something: string; newLength: integer): string;

// Damit ich die Konvertierung nicht mehr falsch mache (Siehe Fehler 1)
function toReal(sToReal: string): real;

// Uberprüfungsmethoden
function canBeReal(text: string): boolean;
function containsChar(text: string; key: char): boolean;

// Verweis zu Zellen(X,Y) Konvertierung
function getDestinationXFromText(text: string): integer;
function getDestinationYFromText(text: string): integer;

implementation

uses sysutils;

function getStringToLength(something: string; newLength: integer; cutString: boolean): string;
var
  i: integer;
begin // Füllt den Lehrraum des Strings mit Leerzeichen
  for i := (length(something)+1) to newLength do something := something + ' ';
  if cutString then result := something[1..newLength] // Gebe den String zurechtgeschnitten zurück
  else result := something; // Gebe den String normal zurück, wenn gewollt
end;

function getStringToLength(something: string; newLength: integer): string;
begin // Gibt seine Argumente nur an die obere Klasse weiter
  result := getStringToLength(something, newLength, true);
end;

function getLengthOfInteger(number: integer): integer;
begin // Macht eine dreckige Konvertierung in einer Methode, damit ich sie nicht so oft sehen muss
  result := length(inttostr(number));
end;

function isAllowedInsideReal(key: char): boolean;
begin // prüft ob ein Zeichen in einem Real vorkommen kann
  case key of
  '0','1','2','3','4','5','6','7','8','9': result := true;
  '.', ',', '-': result := true;
  else result := false;
  end;
end;
      // Datentyp wird gemeint
function canBeReal(text: string): boolean; // habe ich mit einer Exception nicht hinbekommen
var
  i: integer;
begin // überprüft String, ob er in Real Konvertiert werden kann
  result := true;
  for i := 1 to length(text) do
  begin
    if not(isAllowedInsideReal(text[i])) then result := false;
  end;
end;

function containsChar(text: string; key: char): boolean;
var
  i: integer;
begin // Prüft, ob ein String ein Zeichen enthält
  result := false;
  for i := 1 to length(text) do
    if text[i] = key then result := true;
end;

function toReal(sToReal: string): real;
begin // Standart Konvertierung
  result := StrToFloat(sToReal);
end;

function getDestinationXFromText(text: string): integer;
begin // Konvertiere Buchstaben von Beschriftung in X Position von Tabelle
  result := integer(uppercase(text[2])[1])-64;
end;

function getDestinationYFromText(text: string): integer;
var
  i: integer;
  s: string;
begin // Konvertiere Zahl aus Beschiftung in Y Position von Tabelle
  if (length(text) = 3) then
  result := strtoint(text[3])
  else begin
    s := '';
    for i := 3 to length(text) do
      s := s + text[i];
    result := strtoint(s)
  end;
end;

end.
