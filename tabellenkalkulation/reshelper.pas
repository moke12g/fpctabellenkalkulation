unit reshelper;

// Klasse für das helfen beim finden der Terminalauflösung

{$mode objfpc}{$H+}

interface

procedure show();

implementation

uses
  Classes, SysUtils, crt, tools, de;

procedure show();
var
  key: char;
  i, j: integer;
begin
  writeln('Wenn Sie ihre Terminal Aufl', de.oe(),'sung schon parat haben, dr', de.ue(),'cken Sie bitte "q", andernfalls wird ihnen der Assistent bei der Bestimmung dieser helfen. Dr', de.ue(),'cken Sie dazu eine beliebige Taste');
  key := crt.readKey(); // mit 'q' kann der Assistent übersprungen werden
  if not(key = 'q') then begin
  // Beginn vom Assistent
    for i := 1 to 99 do begin // Vertikal
      writeln(100 -i); // Zähle von 99 bis 1 herunter und gebe die Ergebnisse mit neuen Zeilen aus
    end;

    for i := 1 to 100 do begin // Horizontal
      if(((i +1) mod 10) = 0) then write(i+1) // Probiere eine horizontale Hilfe zu integrieren
      else if not(((i mod 10) = 0) or (i = 98)) then write(' '); // Zähle bis 100 in positionierten Zehnerschritten durch
    end; // Hierbei ist es wichtig, dass die 0 von z.B. 10 im zehnten Zeichen steht
     writeln();
    for i := 0 to 10 do begin
      for j := 0 to 9 do begin
        if not((i = 0) and (j = 0)) then write(j); // Zähle von 1 bis 9 -> 0 bis 9
      end;
    end;
    writeln();
    writeln();
  end;
end;

end.
