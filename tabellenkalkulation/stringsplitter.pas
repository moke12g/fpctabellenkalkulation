unit stringsplitter;

// Die Klasse ist nicht gut programmiert, aber das weiß ich auch.
// Das Problem war, dass ich https://www.freepascal.org/docs-html/rtl/sysutils/tstringhelper.split.html verwenden wollte,
// jedoch habe ich es nicht hinbekommen, dass dies mit der alten FPC Version der Schule funktionierte
// und weil das ganze nicht Backporten wollte habe ich es neu implementiert.

{$mode objfpc}{$H+}

interface // TODO: Kommentieren

procedure setString(text: string);
procedure setSeparator(c: char);
procedure process();
function countOccurances(): integer;
function getLength(): integer;
function getSplitAt(index: integer): string;

implementation

uses
  Classes, SysUtils;

var
  text: string;
  separator: char;
  //TODO: Implement chars to ignore
  splits: Array of String;

procedure setString(text: string); // Setzt den Text, mit dem gearbeitet werden soll
begin
  stringsplitter.text := text;
end;

procedure setSeparator(c: char); // Setzt den Separator
begin
  separator := c;
end;

procedure process(); // Startet den Prozess, den String zu teilen
var
  o: integer; // Occurances
  i, io: integer; // io -> letzter Punkt, von dem geteilt wird
// \-> i -> Zählvariable
// es wird also von io bis i geteilt -> alles dazwischen kommt in den neuen String
begin
  o := countOccurances(); // Zähle erstmal, wie oft das Zeichen vorkommt und setze die Länge von splits
  setLength(splits, o+1); //TODO: mehr Code, fals das letzte Feld leer sein sollte

  io := 0; // virtueller erste Teiler ist auf Zeichen 0
  splits[io] := ''; // wird initialisiert, weil jedes weitere Zeichen angehangen wird
  for i := 1 to length(text) do
  begin // für jeden char
    if text[i] = separator then begin
      io += 1; // wenn char ein trenner ist, dann addiere 1 zu io
      splits[io] := ''; // setze den Platz im Array auf ''
      end
    else splits[io] += text[i]; // wenn char kein Trenner ist, wird er an den [io]ten split angehangen
  end; // mache das gansze, bis der String bis nach hinten durchgegangen wurde
end;

function countOccurances(): integer;
var
  i: integer;
begin
  result := 0;
  for i := 1 to length(text) do
  begin // addiere 1 zum Rückgabewert, wenn der Trenner gefunden wurde
    if text[i] = separator then result += 1;
  end;
end;

// Getters

function getLength(): integer;
begin
  result := length(splits);
end;

function getSplitAt(index: integer): string;
begin
  result := splits[index];
  end;

end.

