unit debugmenu;

// Das ist ein ungenutztes Debugmenü
// Hier hätte ich ein paar Funktionen testen können, wenn ich es gewollt hätte
// Gehört nicht zum Programm, ist nur für den Entwickler, außerdem sind
// versteckte Debugmenüs interessant.

{$mode objfpc}{$H+}

interface

procedure show();

implementation

uses
  Classes, SysUtils, crt;

procedure show();
var
  return: boolean;
  key: char;
begin
  return := false;
  while (return = false) do // damit der Nuter das Menü wieder verlassen kann
  begin
  writeln();                // Zeichnet alle Möglichkeiten
  writeln('Debug Menu');
  writeln('0: Back');
  writeln('1: ');
  writeln('2: ');
  writeln('3: ');
  writeln('4: ');
  writeln('5: ');
  key := crt.readkey();      // Liest Input
  case key of                // Wertet Input aus
  '1':writeln();
  else return := true;       // 0 und alle nicht vergebenen wirft den Anwender aus dem Menü
  end;
end;
  end;

end.

