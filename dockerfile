FROM debian:stretch
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install fp-compiler --no-install-recommends -y
COPY tabellenkalkulation/ tabellenkalkulation
WORKDIR tabellenkalkulation/
RUN fpc pTabellenkalkulation.lpr
