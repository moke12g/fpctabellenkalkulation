# Befehle

Var = Variable

ZB = Zellenbezeichnung (z.B. $A7)

| Funktion | Befehl |
| -------- | -------- |
| Beendet das Programm | !q |
| Zeigt die Hilfe | !h |
| Setzt Variable von Zelle | =[Var] |
| Setzt Verweis auf Zelle | =$[ZB] |
| Setze Rechnung | =1;+;2 |
| Setze Rechnung mit Verweis | =1;+;$B2 |
| Setze Division | =7;/;2 |
| Setze Zeichenkette | =FPCT |

Rechenzeichen sind +,-,*,/,m,d
(Addition, Subtraktion, Division, Mod, Div) (letztere Runden vorherige Ergebnisse und durch die Zelle geladene Zahlen vor dem Rechnen)

Mit Zeichenketten kann nicht gerechnet werden. Rechnen ist nur mit Zahlen und Rechenzeichen möglich.

Hinter jedem "Befehl" muss ein Semikolon stehen, sonnst kann bei der inpretation ein Fehler auftreten.

Bitte vermeiden Sie Leerzeichen in Rechnungen.

Alle Werte in Zellen werden gekürzt, damit sie in die Zelle passen, beim setzen von Zeichenketten sollte diese manuell getrennt werden.

Beachten Sie außerdem, dass Punkt vor Strich Regeln beim Rechnen nicht beachtet werden.


# Markierung (Cursor)

| Funktion | Taste |
| -------- | -------- |
| Nach oben | k,w |
| Nach unten | j,s |
| Nach links | r,a |
| Nach rechts | l,d |
